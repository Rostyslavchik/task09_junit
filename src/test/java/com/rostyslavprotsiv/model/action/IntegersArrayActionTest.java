package com.rostyslavprotsiv.model.action;

import com.rostyslavprotsiv.model.entity.IntLocation;
import com.rostyslavprotsiv.model.exception.IntLocationLogicalException;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class IntegersArrayActionTest {
    private static int[] arr;
    private static final IntegersArrayAction INTEGERS_ARRAY_ACTION
            = new IntegersArrayAction();
    static IntLocation location;

    @Mock
    IntLocation someLocation;
//    @Spy
//    IntLocation someLocation = new IntLocation(5, 8, 12);

    public IntegersArrayActionTest() throws IntLocationLogicalException {
    }

//    IntLocation emptyLocation;
//    IntLocation emptyLocation = mock(IntLocation.class);

    @BeforeAll
    static void setUp() {
        arr = new int[]{-3, 0, 0, -4, 0, 0, 0, -6, 5, 5, 5, 5, 5, -6};
        try {
            location = new IntLocation(5, 8, 12);
        } catch (IntLocationLogicalException e) {
            e.printStackTrace();
        }
    }

    @Disabled("Testing another functionality")
    @Test
    @DisplayName("Testing main functionality")
    void testGetLongestContiguousEqualElementsSequenceInfo() {
        try {
            assertEquals(location, INTEGERS_ARRAY_ACTION
                    .getLongestContiguousEqualElementsSequenceInfo(arr));
        } catch (IntLocationLogicalException e) {
        }
    }

    @RepeatedTest(10)
    void testIntLocationLogicalException() {
        try {
            assertTrue(location.equals(INTEGERS_ARRAY_ACTION
                    .getLongestContiguousEqualElementsSequenceInfo(arr)));
        } catch (IntLocationLogicalException e) {
            e.printStackTrace();
        }
    }

    @Test
    void testConstant() {
        Class<IntegersArrayAction> clazz = IntegersArrayAction.class;
        Integer value;
        try {
            Field start = clazz.getDeclaredField("START");
            start.setAccessible(true);
            value = (Integer) start.get(null);
            assertEquals(1, value);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Test
    void testMethodForEmptyArr() {
//        Assumptions.assumeFalse(System.getProperty("os.name").contains("Windows"));
        IntLocation emptyLocation = new IntLocation();
        int[] emptyArr = new int[0];
        int[] emptyArr1 = new int[]{};
        assertAll("Can't work with empty array",
                () -> assertEquals(emptyLocation, INTEGERS_ARRAY_ACTION
                        .getLongestContiguousEqualElementsSequenceInfo(
                                emptyArr)),
                () -> assertEquals(emptyLocation, INTEGERS_ARRAY_ACTION
                        .getLongestContiguousEqualElementsSequenceInfo(
                                emptyArr1)));

    }

    @Test
    void testNumberBeforeAndAfterAreLess() {
        assertNotNull(someLocation);
        when(someLocation.getStartPosition()).thenReturn(8);
//        doReturn(8).when(someLocation).getStartPosition();
        when(someLocation.getEndPosition()).thenReturn(12);
        assertTrue(INTEGERS_ARRAY_ACTION
                .numberBeforeAndAfterAreLess(arr, someLocation));
        verify(someLocation, times(3)).getStartPosition();
    }
}
