package com.rostyslavprotsiv.model.creator;

import com.rostyslavprotsiv.model.exception.MinesweeperCreatorLogicalException;
import org.apache.logging.log4j.core.util.JsonUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MinesweeperCreatorTest {
    private static MinesweeperCreator creator;
    private static int M;
    private static int N;
    private static int p;

    @BeforeAll
    static void setUp() {
        creator = new MinesweeperCreator();
        M = 6;
        N = 6;
        p = 40;
    }

    @Test
    void testFieldGenerating() {
        int numOfBombs = 0;
        int numOfBombsInTheory = (int)((M * N) * ((double)p / 100));
        try {
            boolean[][] field = creator.generateField(M, N, p);
            for (int i = 0; i < M; i++) {
                for (int j = 0; j < N; j++) {
                    if (field[i][j]) {
                        numOfBombs++;
                    }
                }
            }
            assertTrue(numOfBombs <= numOfBombsInTheory + 2
                    && numOfBombs >= numOfBombsInTheory - 2);
        } catch (MinesweeperCreatorLogicalException e) {
            e.printStackTrace();
        }
    }
}