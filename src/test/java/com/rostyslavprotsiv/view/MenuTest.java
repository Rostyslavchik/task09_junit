package com.rostyslavprotsiv.view;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class MenuTest {

    @Spy
    Menu menu;

    @Test
    void testOutReadyField() {
        menu.outReadyField(new boolean[][] {{true, true,}, {false, true},
                {true,false}, {false, true}});
        verify(menu, times(3)).asterisksNearby(
                any(boolean[][].class), anyInt(), anyInt());
    }

}