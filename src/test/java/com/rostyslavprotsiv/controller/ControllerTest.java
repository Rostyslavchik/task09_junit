package com.rostyslavprotsiv.controller;

import com.rostyslavprotsiv.model.exception.MinesweeperCreatorLogicalException;
import com.rostyslavprotsiv.view.Menu;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ControllerTest {

    @Mock
    Menu menu;

    @InjectMocks
    Controller controller;

    @Test
    void testExecute() {
        try {
            doCallRealMethod().when(menu).outArr(any());
            doAnswer((i) -> {
                System.out.println("Welcome");
                return null;
            }).when(menu).welcome();
            controller.execute(6, 6, 40);
            verify(menu, times(1)).welcome();
        } catch (MinesweeperCreatorLogicalException e) {
            e.printStackTrace();
        }
    }
}