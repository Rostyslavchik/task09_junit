package com.rostyslavprotsiv.model.exception;

public class MinesweeperCreatorLogicalException extends Exception {
    public MinesweeperCreatorLogicalException() {
    }

    public MinesweeperCreatorLogicalException(String s) {
        super(s);
    }

    public MinesweeperCreatorLogicalException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public MinesweeperCreatorLogicalException(Throwable throwable) {
        super(throwable);
    }
}
