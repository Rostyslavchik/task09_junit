package com.rostyslavprotsiv.model.exception;

public class IntLocationLogicalException extends Exception {
    public IntLocationLogicalException() {}

    public IntLocationLogicalException(String message, Throwable cause) {
        super(message, cause);
    }

    public IntLocationLogicalException(String message) {
        super(message);
    }

    public IntLocationLogicalException(Throwable cause) {
        super(cause);
    }
}
