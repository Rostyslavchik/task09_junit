package com.rostyslavprotsiv.model.entity;

import com.rostyslavprotsiv.model.exception.IntLocationLogicalException;

public class IntLocation implements Comparable<IntLocation> {
    private int length;
    private int startPosition;
    private int endPosition;

    public IntLocation() {}

    public IntLocation(int length, int startPosition, int endPosition)
                            throws IntLocationLogicalException {
        if (checkLength(length)) {
            this.length = length;
        } else {
            throw new IntLocationLogicalException("Length is less then 0;");
        } if (checkPositions(startPosition, endPosition)) {
            this.startPosition = startPosition;
            this.endPosition = endPosition;
        } else {
            throw new IntLocationLogicalException("Positions are bad");
        }
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) throws IntLocationLogicalException {
        if (checkLength(length)) {
            this.length = length;
        } else {
            throw new IntLocationLogicalException("Length is less then 0;");
        }
    }

    public int getStartPosition() {
        return startPosition;
    }

    public void setStartPosition(int startPosition)
                                      throws IntLocationLogicalException {
        if (checkPositions(startPosition, endPosition)) {
            this.startPosition = startPosition;
        } else {
            throw new IntLocationLogicalException("Positions are bad");
        }
    }

    public int getEndPosition() {
        return endPosition;
    }

    public void setEndPosition(int endPosition)
                                    throws IntLocationLogicalException {
        if (checkPositions(startPosition, endPosition)) {
            this.endPosition = endPosition;
        } else {
            throw new IntLocationLogicalException("Positions are bad");
        }
    }

    private boolean checkLength(int length) {
        return length > 0;
    }

    private boolean checkPositions(int startPosition, int endPosition) {
        return startPosition >= 0 && startPosition <= endPosition
                && (endPosition - startPosition + 1) == length;
    }

    @Override
    public int compareTo(IntLocation obj) {
        return obj.length - this.length;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } if (obj == null) {
            return false;
        } if (getClass() != obj.getClass()) {
            return false;
        }
        IntLocation other = (IntLocation) obj;
        if (length != other.length) {
            return false;
        }
        if (startPosition != other.startPosition) {
            return false;
        }
        if (endPosition != other.endPosition) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 34;
        int result = 1;
        result = prime * result + length; //3 r = 37   4 r = 38
        result = prime * result + startPosition; //4 r = 34 * 37 + 4   3 r = 34 * 38 + 3
        result = prime * result + endPosition; //5 r = up + 5   5
        return result;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "@" + "length = " + length
                + ", startPosition = " + startPosition + ", endPosition = "
                + endPosition;
    }

}
