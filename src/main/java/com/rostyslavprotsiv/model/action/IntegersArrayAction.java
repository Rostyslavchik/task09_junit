package com.rostyslavprotsiv.model.action;

import com.rostyslavprotsiv.model.entity.IntLocation;
import com.rostyslavprotsiv.model.exception.IntLocationLogicalException;

import java.util.*;

public class IntegersArrayAction {
    static final Integer START = 1;

    public IntLocation getLongestContiguousEqualElementsSequenceInfo(
            int[] arr) throws IntLocationLogicalException {
        List<IntLocation> sequences = getSequences(arr);
        Collections.sort(sequences);
        Optional<IntLocation> result = sequences.parallelStream()
                .filter((elem) -> numberBeforeAndAfterAreLess(arr, elem))
                .findFirst();
        return result.orElse(new IntLocation());
    }

    public boolean numberBeforeAndAfterAreLess(int[] arr, IntLocation location) {
        if (location.getStartPosition() != 0
                && location.getEndPosition() != arr.length - 1) {
            int numberBefore = arr[location.getStartPosition() - 1];
            int numberAfter = arr[location.getEndPosition() + 1];
            int number = arr[location.getStartPosition()];
            return numberBefore < number && numberAfter < number;
        } else {
            return false;
        }
    }

    private List<IntLocation> getSequences(int[] arr)
            throws IntLocationLogicalException {
        List<IntLocation> sequences = new LinkedList<>();
        int firstPosition = -1;
        int secondPosition = -1;
        for (int i = START; i < arr.length ; i++) {
            if (arr[i - 1] == arr[i]) {
                secondPosition = i;
                if (firstPosition == -1) {
                    firstPosition = i - 1;
                }
            } else {
                if (firstPosition != -1) {
                    sequences.add(new IntLocation(secondPosition
                            - firstPosition + 1, firstPosition, secondPosition));
                    firstPosition = -1;
                    secondPosition = -1;
                }
            }
        }
        return sequences;
    }
}
