package com.rostyslavprotsiv.model.creator;

import com.rostyslavprotsiv.model.exception.MinesweeperCreatorLogicalException;

import java.util.Random;

public class MinesweeperCreator {
    private static final int MIN_P = 0;
    private static final int MAX_P = 100;

    public boolean[][] generateField(int M, int N, int p) throws
                                    MinesweeperCreatorLogicalException {
        if (!checkP(p)) {
            throw new MinesweeperCreatorLogicalException("P is too low");
        } else if (!checkMN(M, N)) {
            throw new MinesweeperCreatorLogicalException("M or N <= 0");
        }
        boolean[][] field = new boolean[M][N];
        Random random = new Random();
        int temp;
        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++) {
                temp = random.nextInt(MAX_P);
                if (temp <= p) {
                    field[i][j] = true;
                } else {
                    field[i][j] = false;
                }
            }
        }
        return field;
    }

    private boolean checkP(int p) {
        return p > MIN_P && p < MAX_P;
    }

    private boolean checkMN(int M, int N) {
        return M > 0 && N > 0;
    }
}
