package com.rostyslavprotsiv.view;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Menu {
    private static final Logger LOGGER = LogManager.getLogger(Menu.class);

    public void welcome() {
        LOGGER.info("Hi, welcome to my Minesweeper game!");
    }

    public void outArr(boolean[][] field) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < field.length; i++) {
            builder.append("\n|");
            for (int j = 0; j < field[i].length; j++) {
                if (!field[i][j]) {
                    builder.append(" |");
                } else {
                    builder.append("*|");
                }
            }
        }
        LOGGER.info(builder.toString());
    }

    public void outReadyField(boolean[][] field) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < field.length; i++) {
            builder.append("\n|");
            for (int j = 0; j < field[i].length; j++) {
                if (!field[i][j]) {
                    builder.append(asterisksNearby(field, i, j) + "|");
                } else {
                    builder.append("*|");
                }
            }
        }
        LOGGER.info(builder.toString());
    }

    public int asterisksNearby(boolean[][] field, int row, int column) {
        int temp = 0;
        if (field.length != row + 1 && field[row + 1][column]) {
            temp++;
        } if (field[row].length != column + 1 && field[row][column + 1]) {
            temp++;
        } if (row != 0 && field[row - 1][column]) {
            temp++;
        } if (column != 0 && field[row][column - 1]) {
            temp++;
        } if (field.length != row + 1 && field[row].length != column + 1
                && field[row + 1][column + 1]) {
            temp++;
        } if (field.length != row + 1 && column != 0
                && field[row + 1][column - 1]) {
            temp++;
        } if (row != 0 && field[row].length != column + 1
                && field[row - 1][column + 1]) {
            temp++;
        } if (row != 0 && column != 0 && field[row - 1][column - 1]) {
            temp++;
        }
        return temp;
    }
}
