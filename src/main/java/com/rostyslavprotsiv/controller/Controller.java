package com.rostyslavprotsiv.controller;

import com.rostyslavprotsiv.model.creator.MinesweeperCreator;
import com.rostyslavprotsiv.model.exception.MinesweeperCreatorLogicalException;
import com.rostyslavprotsiv.view.Menu;

public class Controller {
    private static final MinesweeperCreator MINESWEEPER_CREATOR =
            new MinesweeperCreator();
//    private static final Menu MENU = new Menu();
    private static Menu MENU = new Menu();

    public Controller() {}

    public Controller(Menu menu) {
        MENU = menu;
    }

    public void execute(int M, int N, int p)
            throws MinesweeperCreatorLogicalException {
        boolean[][] field = MINESWEEPER_CREATOR.generateField(M, N, p);
        MENU.welcome();
        MENU.outArr(field);
        MENU.outReadyField(field);
    }
}
