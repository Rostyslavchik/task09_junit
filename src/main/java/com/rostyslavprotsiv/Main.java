package com.rostyslavprotsiv;

import com.rostyslavprotsiv.controller.Controller;
import com.rostyslavprotsiv.model.exception.MinesweeperCreatorLogicalException;

public class Main {
    public static void main(String[] args) {
        Controller controller = new Controller();
        try {
            controller.execute(Integer.parseInt(args[0]),
                                Integer.parseInt(args[1]),
                                Integer.parseInt(args[2]));
        } catch (MinesweeperCreatorLogicalException e) {
            e.printStackTrace();
        }
    }
}
